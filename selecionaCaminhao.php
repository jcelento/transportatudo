<body>
  <section class="hero is-fullheight has-text-centered">
    <div class="hero-body">
      <div class="container is-fluid">
        <h1 class="title">
          Resultado
        </h1>   
        <?php
        if(!empty($_GET['idCaminhao'])){
          $caminhao = $_GET['idCaminhao'];
          require_once("db.php");          
          $sql = "SELECT * FROM Caminhao WHERE idCaminhao = '$caminhao' ORDER BY idCaminhao DESC";
          $result = mysqli_query($conn,$sql);
          
          if (mysqli_connect_errno()) {
            printf("Connect failed: %s\n", mysqli_connect_error());
            exit();
          }
          
          if (empty($result)) {
            echo "Caminhao não existe";
            exit;
          }
          if (!empty($_GET['sucess'])) {
            $result = $_GET['sucess'];
            if($result == false){
            echo "Não foi possivel alocar a carga";
            }
            exit;
          }

          ?>

          <div class="container-fluid">
            <table border="1" class=" table table-hover table-striped ">
              <thead>
                <tr>
                  <td>Id do Caminhao</td>  
                  <td>Placa</td>
                  <td>Capacidade</td>
                </tr>
              </thead>
              <tbody>
                <?php while($row = mysqli_fetch_array($result)) { ?>
                  <tr>
                    <td><?php echo $row['idCaminhao']; ?></td>
                    <td><?php echo $row['placa']; ?></td>
                    <td><?php echo $row['capacidade']; ?></td>
                    </tr>
                <?php } ?>
                <?php include 'selecionaCarga.php';?>
              </tbody>
            </table>
          </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </section>
</body>
</html>